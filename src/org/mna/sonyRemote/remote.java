package org.mna.sonyRemote;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.json.JSONException;
import org.json.JSONObject;

public class remote {

	protected Shell shell;
	private Canvas canvas;
	private ScrolledComposite scrolledComposite;
	private Combo comboScale;
	private float scale = 1.0f;
	private Rectangle imageRect = new Rectangle(0,0,0,0);
	private Image image;
	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			remote window = new remote();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(508, 300);
		shell.setText("SWT Application");
		shell.setLayout(new GridLayout(1, false));

		Composite composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
				1, 1));
		RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
		rl_composite.fill = true;
		composite.setLayout(rl_composite);

		comboScale = new Combo(composite, SWT.READ_ONLY);
		String items[] = { "10%", "25%", "50%", "75%", "100%" };
		comboScale.setItems(items);
		comboScale.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					scale = (new DecimalFormat("0.0#%").parse(comboScale.getText())).floatValue();
					canvas.setSize((int)(imageRect.width* scale),(int)(imageRect.height*scale));
					canvas.redraw();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		@SuppressWarnings("unused")
		Spinner spinerDelay = new Spinner(composite, SWT.BORDER);
		spinerDelay.setEnabled(false);
		spinerDelay.setTouchEnabled(true);

		Button btnTimeControl = new Button(composite, SWT.CHECK);
		btnTimeControl.setEnabled(false);
		btnTimeControl.setText("Zeit Steuerung");

		Button btnTakePhoto = new Button(composite, SWT.NONE);
		btnTakePhoto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {

				try {
					postHttpMessage("{\"method\":\"startRecMode\",\"params\":[],\"id\":5}");
					setImageByJasonMessage(postHttpMessage("{\"method\":\"actTakePicture\",\"params\":[],\"id\":18}"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				canvas.redraw();

			}
		});
		btnTakePhoto.setText("Auslösen");

		scrolledComposite = new ScrolledComposite(shell,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		GridData gd_scrolledComposite = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		gd_scrolledComposite.heightHint = 222;
		gd_scrolledComposite.widthHint = 467;
		scrolledComposite.setLayoutData(gd_scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		canvas = new Canvas(scrolledComposite, SWT.H_SCROLL
				| SWT.V_SCROLL);
		canvas.addPaintListener(new PaintListener() {

			public void paintControl(PaintEvent e) {
				if (image == null)
				{
					return;
				}
				Display display = Display.getCurrent();
				Transform tr = new Transform(display);
				tr.scale(scale, scale);
				imageRect = image.getBounds();
				e.gc.setTransform(tr);
				e.gc.drawImage(image, 0, 0);
				tr.dispose();

			}
		});
		scrolledComposite.setContent(canvas);
		scrolledComposite.setMinSize(canvas.computeSize(SWT.DEFAULT,
				SWT.DEFAULT));

	}


	public String postHttpMessage(String param) throws IOException, JSONException{
		String urlParameters = param;
		String request = "http://192.168.122.1:8080/sony/camera";
		URL url = new URL(request); 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false); 
		connection.setRequestMethod("POST"); 
		connection.setRequestProperty("Content-Type", "application/json"); 
		connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		connection.setUseCaches (false);

		DataOutputStream writer = new DataOutputStream(connection.getOutputStream ());
		writer.writeBytes(urlParameters);
		writer.flush();

		String rc = new String();

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(connection.getInputStream()) );

		for ( String line; (line = reader.readLine()) != null; )
		{
			rc += line;
			System.out.println( line );
		}

		connection.disconnect();	
		writer.close();
		reader.close();
		return rc;
	}
	public void setImageByJasonMessage(String jMessage)
	{
		JSONObject jObj;

		try {
			Display display = Display.getCurrent();
			jObj = new JSONObject(jMessage);
			final String resultSTr = jObj.get("result").toString();
			final URL tmpURL = new URL(resultSTr.substring(3, resultSTr.length()-3));
			image = new Image(display, tmpURL.openStream());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
