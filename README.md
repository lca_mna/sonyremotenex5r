# Sony Remote Control

Dieses Projekt dient zur Fernsteuerung einer Sony NEX-5R Kamera. Es ist aus
dem wunsch gebohren, die Kamera auch von einem einfachen Rechner aus zu
bedienen.

## Funktionsumfang

Auslosen eines Photos uber den PC und anzeigen des Photos am PC. Die Anzeige
ist skalierbar.

### Screenshot:

![Geoffnete Anwendung mit einem Bild][1]

## Vorrausetzungen

- Computer/Laptop

- Die Applikation Smart-Fernbedienung ist gestartet (erhaltlich uber
PlayMemories)

- Wlan Verbindung zur Kamera ist aufgebaut

## Start der Anwendung

java -jar sonyRemote_x86_64.jar

(Ein lauffahiges Jar-Archiv ist im Download-Bereich zu finden)

## Roadmap

- JSON von [http://json.org/java/][2] korrekt in das Projekt einzubinden

- Zeitgesteuerte Aufnahmen

- Scrollbars korrekt einbinden

- Live view

## Moglichkeitnen der Mitarbeit

Wer intersse an diesem Projekt hat ist gerne eingeladen hier mitzuwirken.

Die anwendung ist mit Java mit Eclips geschrieben. Fur die GUI kommt SWT zum
Einsatz.

### Hinweise zu Markenzeichen:

PlayMemories ist Markenzeichen der Sony Corporation

   [1]: https://bitbucket.org/lca_mna/sonyremotenex5r/raw/5551973872d9364f04126f9bf83da96ec3303d0f/screenshot.jpg

   [2]: http://json.org/java/

